#Progmram
import numpy as np
from sht_py import sht
import time
jx, kx = 128*2, 256*2

ymin = 0
ymax = np.pi
dy = (ymax-ymin)/float(jx)
y = np.linspace(ymin+0.5*dy,ymax-0.5*dy,jx)

zmin = -np.pi
zmax = np.pi
dz = (zmax-zmin)/float(kx)
z = np.linspace(zmin+0.5*dz,zmax-0.5*dz,kx)

yy, zz = np.meshgrid(y,z,indexing="ij")

#qq = 1./np.sqrt(4.*np.pi)*np.ones((jx,kx)) # l=0, m=0
#qq = np.sqrt(3.  /4. /np.pi)*np.cos(yy) # l=1, m=0
#qq = np.sqrt(5.  /16./np.pi)*np.sin(yy)*np.exp((0.+1.j)*zz) # l=1, m=1
#qq = np.sqrt(5.  /16./np.pi)*(3.*np.cos(yy)**2-1.) # l=2, m=0
#qq = np.sqrt(15. /8. /np.pi)*np.sin(yy)*np.cos(yy)*np.exp((0.+1.j)*zz) # l=2, m=1
#qq = np.sqrt(15. /32./np.pi)*np.sin(yy)**2*np.exp((0.-2.j)*zz) # l=2, m=2
#qq = np.sqrt(7.  /16./np.pi)*(5.*np.cos(yy)**3 - 3.*np.cos(yy)) # l=3, m=0
#qq = np.sqrt(21. /64./np.pi)*np.sin(yy)*(5.*np.cos(yy)**2 - 1.)*np.exp((0.+1.j)zz) # l=3, m=1
#qq = np.sqrt(105./32./np.pi)*np.sin(yy)**2*np.cos(yy)*np.exp((0.+2.j)*zz) # l=3, m=2
#qq = np.sqrt(35./64. /np.pi)*np.sin(yy)**3*np.exp((0.-3.j)*zz) # l=3, m=3
qq = np.sqrt(35./64. /np.pi)*np.sin(yy)**3*((0.+0.j)*cos(3.*z)+(0.+1.j)*sin(3.*z)) # l=3, m=3
#qq = 1./1024.*np.sqrt(969969./np.pi)*np.sin(yy)**10*np.exp((0.-10.j)*zz) # l=10,m=10

stime = time.time()
fqq = sht(qq,y,z)
etime = time.time()

print((np.real(fqq[3,3]),np.imag(fqq[3,3])))
print(etime-stime)
