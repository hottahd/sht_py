# sht_py

pythonによる球面調和関数のコード

sht_py.pyに関数がある。test.pyでテスト。
```python
from sht_py
```
とすれば使える。

y[jx]: 余緯度
z[kx]:　経度
qq[jx,kx]: 物理量
kxはjxの2倍にするという制約がある。

shtは、主に解析のために使う。負のmは適切に足してしまい、kx/4までを返す。
shtmは逆変換もしたいときに使う。direction=1が順変換(デフォルト)、direction=-1が逆変換となる。
単純に
```python
fqq = sht_py.shtm(qq,y,z) 
ffqq = sht_py.shtm(fqq,y,z,direction=-1)
```
とすればqqとffqqは球面調和関数展開の誤差の範囲で一致する。